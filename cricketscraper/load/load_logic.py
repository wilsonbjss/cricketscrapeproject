"""Logic for loading clean data onto SQL database"""
import pandas as pd
import sqlalchemy


def load(clean_data: pd.DataFrame, db_url: str) -> None:
    """Logic to load cleaned pd.DataFrame to sqlite database

    Args:
        clean_data (pd.DataFrame): DataFrame containing transformated data
        db_url (str): URL/path to target database
    """
    engine = sqlalchemy.create_engine(db_url)
    connection = engine.connect()
    clean_data.to_sql("batter_stats", connection, if_exists="replace")
    connection.close()
