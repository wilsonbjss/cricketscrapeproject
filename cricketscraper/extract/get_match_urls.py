"""Function for getting list of match URLs from fixtures page"""
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By


def get_match_urls(driver: WebDriver, fixtures_url: str) -> list:
    """Function for generating list of URLs to individual match scorecards from fixtures page.

    Args:
        driver (WebDriver): Provided webdriver object from get_driver function

    Returns:
        list: List of URLs leading to match scorecards
    """

    driver.get(fixtures_url)
    match_link_elems = driver.find_elements(
        By.CSS_SELECTOR, 'a[href*="/full-scorecard"][href*="indian-premier-league"]'
    )
    return [item.get_attribute("href") for item in match_link_elems]
