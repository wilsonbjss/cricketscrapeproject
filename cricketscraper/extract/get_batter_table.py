"""Function for scraping batter tables using a given url and webdriver"""
from selenium.webdriver.chrome.webdriver import WebDriver
import pandas as pd


def get_batter_table(driver: WebDriver, url: str) -> list:
    """Get tables containing batter information from given URL using provided webdriver
    and converting to pandas dataframes.

    Args:
        driver (WebDriver): Provided webdriver for a single browser instance
        url (str): URL to scorecard page

    Returns:
        list: List containing 2 dataframes: 1 dataframe for each team in a given match.
    """
    driver.get(url)
    page_tables = pd.read_html(driver.page_source)
    return [page_tables[0], page_tables[2]]
