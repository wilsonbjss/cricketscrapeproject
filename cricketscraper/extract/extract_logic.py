"""Logic for extracting required data from given source"""
import pandas as pd
from cricketscraper.extract.get_driver import get_driver
from cricketscraper.extract.get_batter_table import get_batter_table
from cricketscraper.extract.get_match_urls import get_match_urls


def extract(fixtures_url: str) -> pd.DataFrame:
    """Logic for extracting all required match details from gien fixtures_url

    Args:
        fixtures_url (str): URL to Fixtures and Results page

    Returns:
        pd.DataFrame: pd.DataFrame containing dirty data for transformation
    """
    driver = get_driver()
    match_urls = get_match_urls(driver, fixtures_url)
    batter_dfs = []
    for url in match_urls:
        batter_dfs.extend(get_batter_table(driver, url))
    return pd.concat(batter_dfs)
