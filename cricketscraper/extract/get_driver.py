"""Module containing function for generating a webdriver object"""
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def get_driver() -> webdriver.Chrome:
    """Function to create and return a chrome webdriver object

    Returns:
        webdriver.Chrome: instance of chrome webdriver object
    """
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    return webdriver.Chrome(options=chrome_options)
