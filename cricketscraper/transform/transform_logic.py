"""Logic for cleaning dirty data from extract function"""
import pandas as pd


def transform(data: pd.DataFrame) -> pd.DataFrame:
    """Cleans + transforms dirty data from extract function
    selecting relevent columns and dropping unwanted rows/columns

    Args:
        data (pd.DataFrame): Dirty data from extract function

    Returns:
        pd.DataFrame: Cleaned data as pd.DataFrame
    """
    # Look to refactor maybe?
    data = data[["BATTING", "R", "B", "4s", "6s", "Unnamed: 1"]]
    data = data.rename(columns={"Unnamed: 1": "Outs"}, inplace=False)
    data = data[data["BATTING"] != data["Outs"]]
    discard_row_keywords = ["Extras", "TOTAL", "Did not bat:", "Fall of wickets:"]
    data = data[~data["BATTING"].str.contains("|".join(discard_row_keywords))]
    data["Outs"] = (data["Outs"] != "not out").astype(int)
    int_cols = ["R", "B", "4s", "6s", "Outs"]
    data[int_cols] = data[int_cols].astype(int)
    data["BATTING"] = data["BATTING"].astype("O")
    data = data.groupby("BATTING", as_index=False).sum()
    return data
