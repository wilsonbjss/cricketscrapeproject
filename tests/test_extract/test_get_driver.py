"""Pytest for get_driver function"""
from selenium.webdriver.chrome.webdriver import WebDriver
from cricketscraper.extract.get_driver import get_driver


def test_get_driver():
    """Verify get_driver function generates and returns webdriver object"""
    chrome_webdriver = get_driver()
    assert isinstance(chrome_webdriver, WebDriver)
