"""Pytest for get_match_urls function"""
import pytest
from cricketscraper.extract.get_match_urls import get_match_urls
from cricketscraper.extract.get_driver import get_driver

# pylint: disable=W0621


@pytest.fixture
def fixture_url():
    """Saves fixtures_url for testing get_match_urls function"""
    return (
        "https://www.espncricinfo.com"
        "/series/indian-premier-league-2022-1298423"
        "/match-schedule-fixtures-and-results"
    )


@pytest.fixture
def chrome_driver():
    """Instantisate chromedriver via get_driver function"""
    return get_driver()


@pytest.fixture
def output(chrome_driver, fixture_url):
    """Run single instance of tested function for all tests"""
    return get_match_urls(chrome_driver, fixture_url)


def test_get_match_urls(output):
    """Verify get_match_urls returns a list as expected"""
    assert isinstance(output, list)


def test_url_contents(output):
    """Verify items in list contain keywords expected in required URL"""
    assert all(
        "https://www.espncricinfo.com/series/indian-premier-league-2022" in item
        for item in output
    ) and all("/full-scorecard" in item for item in output)
