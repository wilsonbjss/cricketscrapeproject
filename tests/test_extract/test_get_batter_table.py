"""Pytest for get_batter_table function and verifying returned objects"""
import pytest
import pandas as pd
from cricketscraper.extract.get_batter_table import get_batter_table
from cricketscraper.extract.get_driver import get_driver

# pylint: disable=W0621


@pytest.fixture
def chrome_driver():
    """Instantiate webdriver via get_driver() function"""
    return get_driver()


@pytest.fixture
def url():
    """Saves URL for testing with live data"""
    return (
        "https://www.espncricinfo.com"
        "/series/indian-premier-league-2022-1298423"
        "/gujarat-titans-vs-rajasthan-royals-final-1312200/full-scorecard"
    )


@pytest.fixture
def output(chrome_driver, url):
    """Pulls tables from given URL and returns them in a list"""
    return get_batter_table(chrome_driver, url)


def test_get_batter_table_type(output):
    """Verify returned object is a list as expected"""
    assert isinstance(output, list)


def test_number_of_tables(output):
    """Verify function returns expected number of tables"""
    assert len(output) == 2


def test_batter_table_item_type(output):
    """Verify items in returned list are pd DataFrames as expected"""
    assert isinstance(output[0], pd.DataFrame) and isinstance(output[1], pd.DataFrame)


def test_batter_table_columns(output):
    """Verify columns in generated tables contain required info"""
    table0_columns = output[0].columns
    table1_columns = output[1].columns
    assert all(
        x in table0_columns for x in ["BATTING", "Unnamed: 1", "R", "B", "4s", "6s"]
    )
    assert all(
        x in table1_columns for x in ["BATTING", "Unnamed: 1", "R", "B", "4s", "6s"]
    )
