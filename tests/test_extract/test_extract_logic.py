"""Pytest for extract_logic and verifying returned outputs"""
import pandas as pd
import pytest
from cricketscraper.extract.extract_logic import extract

# pylint: disable=W0621


@pytest.fixture
def fixture_url():
    """Saves fixtures_url for testing extract function"""
    return (
        "https://www.espncricinfo.com"
        "/series/indian-premier-league-2022-1298423"
        "/match-schedule-fixtures-and-results"
    )


@pytest.fixture
def output(fixture_url):
    """Runs single instance of extract function for all tests"""
    return extract(fixture_url)


def test_extract_logic(output):
    """Verify returned object is pd DataFrame as expected"""
    assert isinstance(output, pd.DataFrame)


def test_output_columns(output):
    """Verify returned objects contains required information"""
    table_columns = output.columns
    assert all(
        x in table_columns for x in ["BATTING", "Unnamed: 1", "R", "B", "4s", "6s"]
    )
