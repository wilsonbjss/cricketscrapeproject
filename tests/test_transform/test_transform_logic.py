"""Pytest for transform_logic function"""
import pytest
import pandas.api.types as ptypes
import pandas as pd
from cricketscraper.transform.transform_logic import transform
from cricketscraper.extract.get_batter_table import get_batter_table
from cricketscraper.extract.get_driver import get_driver

# pylint: disable=W0621


@pytest.fixture
def chrome_driver():
    """Instantiate chrome webdriver object"""
    return get_driver()


@pytest.fixture
def score_url():
    """Instantiate URL string for test_data"""
    return (
        "https://www.espncricinfo.com"
        "/series/indian-premier-league-2022-1298423"
        "/chennai-super-kings-vs-kolkata-knight-riders-1st-match-1304047/full-scorecard"
    )


@pytest.fixture
def test_data(chrome_driver, score_url):
    """Generate single match table for test_data and duplicates it to emulate
    batters appearing in multiple scorecards"""
    return get_batter_table(chrome_driver, score_url)[0]


@pytest.fixture
def output(test_data):
    """Apply cleaning/transforms to test_data so output can be verified"""
    return transform(test_data)


@pytest.fixture
def triple_data(test_data):
    """Creates dataframe from concatenating three output dataframes together
    to emulate batters appearing in multiple scorecards"""
    return pd.concat([test_data, test_data.copy(), test_data.copy()])


def test_column_select_and_rename(output):
    """Verify only relevent columns are selected and 'Out' columns aptly named"""
    assert (output.columns == ["BATTING", "R", "B", "4s", "6s", "Outs"]).all()


def test_drop_bowler_rows(output):
    """Verify erroneous rows filled with bowler details are dropped"""
    assert len(output[output["BATTING"] == output["Outs"]]) == 0


def test_drop_unrequired_rows(output):
    """Verify rows containing irrelevent data are dropped"""
    assert len(output) == 7  # Sample data from given URL has 7 rows


def test_change_outs_to_1or0_int(output):
    """Verify Outs column replaced with 1/0"""
    assert all(x in [0, 1] for x in output["Outs"])


def test_set_dtypes(output):
    """Verify column dtypes set correctly"""
    int_cols = ["R", "B", "4s", "6s", "Outs"]
    obj_cols = "BATTING"
    assert ptypes.is_string_dtype(output[obj_cols]) and all(
        ptypes.is_integer_dtype(output[col]) for col in int_cols
    )


def test_groupby(triple_data, output):
    """Verify groupby sums columns correctly and 'BATTING' column has no duplicates"""
    triple_output = transform(triple_data)
    int_cols = ["R", "B", "4s", "6s", "Outs"]
    # This is scuffed, find a better way, refactor?
    assert triple_output["BATTING"].value_counts().max() == 1 and all(
        all(triple_output[col] == 3 * output[col]) for col in int_cols
    )
