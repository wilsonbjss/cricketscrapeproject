"""Pytest for load_logic, verify loaded data is correct"""
from unittest.mock import patch
import pytest
from cricketscraper.extract.get_driver import get_driver
from cricketscraper.extract.get_batter_table import get_batter_table
from cricketscraper.transform.transform_logic import transform
from cricketscraper.load.load_logic import load

# pylint: disable=W0621


@pytest.fixture
def test_data():
    """Create clean data from single match results for testing purposes"""
    driver = get_driver()
    match_url = (
        "https://www.espncricinfo.com"
        "/series/indian-premier-league-2022-1298423"
        "/chennai-super-kings-vs-kolkata-knight-riders-1st-match-1304047/full-scorecard"
    )
    dirty_data = get_batter_table(driver, match_url)[0]
    return transform(dirty_data)


@patch("cricketscraper.load.load_logic.pd.DataFrame.to_sql")
def test_load_callable(mock_to_sql, test_data):
    """Verify load function called with test_data"""
    load(test_data, "sqlite:///./tests/test_data/test_data.db")
    assert mock_to_sql.called
